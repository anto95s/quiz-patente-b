<html>
    <head>
        <title>Domande</title>
        <link rel="stylesheet" href="css/style.css">
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        
        <link href="https://fonts.googleapis.com/css2?family=Lato&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    </head>
    </html>
    <body>
        <style>
            .bar {
                width: ${requestScope.percentage}%;
            }
        </style>
        <div class="title">Quiz Patente B</div>
        <div class="progress-bar-a">
            <div class="bar"></div>
        </div>
        <div class="main-form">
            <form action="nextQuestion" method="POST">
                <div class="main-title"><b>${requestScope.nextQuestion}</b></div>

                <!-- src="../image/semaforoA.png" -->
                <div class="quest-image"><img src="../image/${requestScope.nextImage}" onerror='this.style.display = "none"'></div>

                <input class="true-input" type="submit" id="fsubmit" name="button" value="Vero">
                <input class="false-input" type="submit" id="fsubmit" name="button" value="Falso">
            </form>
        </div>
        <div class="progress-bar-b">
        </div>
        <div class="question-counter">
            ${sessionScope.realCounter} di ${sessionScope.questionsNumber}
        </div>
    </body>
</html>