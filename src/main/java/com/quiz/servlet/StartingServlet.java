package com.quiz.servlet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.Questions;

import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet("/startingServlet")
public class StartingServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        String name = request.getParameter("fname");
        request.getSession().setAttribute("name", name);

        Questions questions = Questions.getInstance();
        request.getSession().setAttribute("quests", questions);
        String currentQuest = questions.getQuest();

        Boolean correctAnswer = null;
        int errors = 0;
        request.getSession().setAttribute("errors", errors);

        String nextImage = "";
        String jsonString = "";
        try{
            jsonString = new String ( Files.readAllBytes( Paths.get("src/main/resources/static/questions.json") ) );
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject(jsonString);
        JSONArray arr = obj.getJSONArray("quiz");
        for(int i=0; i<arr.length(); i++){
            if(arr.getJSONObject(i).getString("question").equals(currentQuest)) {
                correctAnswer = arr.getJSONObject(i).getBoolean("answer");
                nextImage = arr.getJSONObject(i).getString("imageUrl");
            }
        }

        int counter = 0;

        int realCounter = counter + 1;
        request.getSession().setAttribute("realCounter", realCounter);
        
        int questionsNumber = questions.getSize()+1; //una è stata già tolta, quindi +1
        request.getSession().setAttribute("counter", counter);
        request.getSession().setAttribute("questionsNumber", questionsNumber);

        int percentage = counter*100/questionsNumber;
        request.setAttribute("percentage", percentage);

        request.getSession().setAttribute("correctAnswer", correctAnswer);
        request.setAttribute("nextQuestion", currentQuest);
        request.setAttribute("nextImage", nextImage);

        RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/question.jsp");
        rd.forward(request, response);
   }

   @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // this.doPost(request, response);
        response.getWriter().write("Error 404: Not Found");
        response.flushBuffer();
    }
   
}