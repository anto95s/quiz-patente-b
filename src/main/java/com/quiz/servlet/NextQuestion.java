package com.quiz.servlet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.quiz.Questions;

import org.json.JSONArray;
import org.json.JSONObject;

@WebServlet("/nextQuestion")
public class NextQuestion extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Boolean correctAnswer = (Boolean) request.getSession().getAttribute("correctAnswer");
        System.out.println("Correct Answer: " + correctAnswer);
        String answer = request.getParameter("button");

        int errors = (int) request.getSession().getAttribute("errors");
        System.out.println("Answer: " + answer);
        if((answer.equals("Vero") && correctAnswer == false) || (answer.equals("Falso") && correctAnswer == true)) {
            //RISPOSTA SBAGLIATA
            
            errors++;
            request.getSession().setAttribute("errors", errors);
        }

        Questions questions = (Questions) request.getSession().getAttribute("quests");
        if(questions.getSize() > 0) {
            String currentQuest = questions.getQuest();

            String nextImage = "";
            String jsonString = "";
            try{
                jsonString = new String ( Files.readAllBytes( Paths.get("src/main/resources/static/questions.json") ) );
            } 
            catch (IOException e) {
                e.printStackTrace();
            }
            JSONObject obj = new JSONObject(jsonString);
            JSONArray arr = obj.getJSONArray("quiz");
            for(int i=0; i<arr.length(); i++){
                if(arr.getJSONObject(i).getString("question").equals(currentQuest)) {
                    correctAnswer = arr.getJSONObject(i).getBoolean("answer");
                    nextImage = arr.getJSONObject(i).getString("imageUrl");
                }
            }

            int counter = (int) request.getSession().getAttribute("counter")+1;
            
            int realCounter = counter + 1;
            request.getSession().setAttribute("realCounter", realCounter);

            request.getSession().setAttribute("counter", counter);
            int questionsNumber = (int) request.getSession().getAttribute("questionsNumber");
            int percentage = counter*100/questionsNumber;
            request.setAttribute("percentage", percentage);

            request.getSession().setAttribute("correctAnswer", correctAnswer);
            request.setAttribute("nextQuestion", currentQuest);
            request.setAttribute("nextImage", nextImage);

            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/question.jsp");
            rd.forward(request, response);
        } else {
            System.out.println(errors);
            RequestDispatcher rd = request.getRequestDispatcher("/WEB-INF/results.jsp");
            rd.forward(request, response);
        }

        

        //correctAnswer va quindi riaggiornata e passata nella Session alla prossima Servlet
    }

}
