package com.quiz;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ThreadLocalRandom;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Questions {
    //singleton con array di domande (40 prese casualmente dal .json e tutte diverse,
    //che restano per tutta la cache). Si istanzia una volta a startingServlet, e poi si usa,
    //sempre la stessa istanza
    private static Questions quests = null;
    private ArrayList<String> questsAll = new ArrayList<String>();
    private ArrayList<String> questsLimited = new ArrayList<String>();

    private Questions(int n) throws JSONException, IOException {
        //Estrae dal json tutte le domande, le mette in un array. Un secondo array prende n domande a caso.
        //Al momento del mostrare la domanda nella servlet, essa è una chiave e quindi la uso per prendere
        //gli altri valori dal json (risposta ed eventuale immagine), e la tolgo dal secondo array.
        String jsonString = "";
        try{
            jsonString = new String ( Files.readAllBytes( Paths.get("src/main/resources/static/questions.json") ) );
        } 
        catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject obj = new JSONObject(jsonString);
        JSONArray arr = obj.getJSONArray("quiz");
        for(int i=0; i<arr.length(); i++){
            questsAll.add(arr.getJSONObject(i).getString("question")); //stampa solo le domande
        }
        Collections.shuffle(questsAll);
        //Tolgo n random da questsAll, le metto nel nuovo array. Poi faccio un getQuest che servirà
        //dentro la servlet apposita
        int count = 0;
        while(count<n) {
            int randNum = ThreadLocalRandom.current().nextInt(0,n);
            questsLimited.add(questsAll.get(randNum));
            questsAll.remove(randNum);
            count++;
        }

    }

    public static Questions getInstance() throws JSONException, IOException {
        if(quests == null || quests.getSize() == 0) {
            quests = new Questions(5); //NUMERO DOMANDE
        }

        return quests;
    }

    public String getQuest() {
        //Restituisce ed elimina la prima domanda, che viene presa dalla servlet. L'array si ridimensiona.
        return questsLimited.remove(0);
    }

    public int getSize() {
        return questsLimited.size();
    }

}
