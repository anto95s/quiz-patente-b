package com.quiz;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuizController {

	@GetMapping("/")
	public String index() {
        return "Write '/index.html'";
	}

}
